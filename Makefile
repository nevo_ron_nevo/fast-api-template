
.PHONY: start
start:
	uvicorn listerical_api.main:app --reload --host 0.0.0.0

.PHONY: lint
lint:
	flake8

.PHONY: lint-fix
lint-fix:
	flake8 tests
	black fastapi tests --check

.PHONY: api-tests
api-tests:
	pytest

.PHONY: unit-tests
unit-test:
	pytest 


.PHONY: setup
setup:
	poetry config --local virtualenvs.in-project true
	poetry install
	".venv/Scripts/activate"


.PHONY: setup-linux
setup-linux:
	poetry config --local virtualenvs.in-project true
	poetry install
	source .venv/bin/activate


.PHONY: clean
clean:
	rm -rf build/ dist/ *.egg-info/ .eggs/ .pytest_cache/ .mypy_cache .coverage *.spec test-result.html htmlcov/
	find . -type d -name __pycache__ -exec rm -rf '{}' +